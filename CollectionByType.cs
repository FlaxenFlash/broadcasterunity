﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Assets.Source.Messaging
{
	public class CollectionByType
	{
		private Dictionary<Type, object> _collection = new Dictionary<Type, object>();
		public bool AllowDuplicates { get; set; }

		public void AddItem<T>(T item)
		{
			var type = typeof(T);
			AddItemToCollection(item, type);
		}

		public void AddItem(Type type, object item)
		{
			if (!IsOfType(type, item)) return;

			var addItem = typeof(CollectionByType).GetMethod("AddItemToCollection", BindingFlags.Instance | BindingFlags.NonPublic).MakeGenericMethod(type);

			addItem.Invoke(this, new[] { item, type });
		}

		private void AddItemToCollection<T>(T item, Type type)
		{
			if (!_collection.ContainsKey(type))
			{
				_collection[type] = new List<T>();
			}

			var collection = (List<T>)_collection[type];

			if (!AllowDuplicates && collection.Contains(item))
				return;
			collection.Add(item);
		}

		public bool RemoveItem<T>(T item)
		{
			var type = typeof(T);
			return RemoveItemFromCollection(item, type);
		}

		public bool RemoveItem(Type type, object item)
		{
			if (!IsOfType(type, item)) return false;

			var removeItem = typeof(CollectionByType).GetMethod("RemoveItemFromCollection", BindingFlags.Instance | BindingFlags.NonPublic).MakeGenericMethod(type);

			return (bool)removeItem.Invoke(this, new[] { item, type });
		}

		private bool RemoveItemFromCollection<T>(T item, Type type)
		{
			if (!_collection.ContainsKey(type))
				return false;

			var collection = (List<T>)_collection[type];

			return collection.Remove(item);
		}

		public List<T> GetItems<T>()
		{
			var type = typeof(T);
			return !_collection.ContainsKey(type) ? null : (List<T>)_collection[type];
		}

		private bool IsOfType(Type type, object item)
		{
			return type.IsInstanceOfType(item);
		}
	}
}