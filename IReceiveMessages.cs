﻿using Assets.Source.Messaging.Messages;

namespace Assets.Source.Messaging
{
    public interface IReceiveMessages<T> where T : GameMessage
    {
        void HandleMessage(T message);
    }
}